# Requirements

- [node.js](https://nodejs.org/en/)
- [git](https://git-scm.com/)

# Installation

**1.** `git clone https://gitlab.com/milan44/chan-watcher.git`  
**2.** `cd chan-watcher`  
**3.** `npm install`  

# Example Usage

```
node main.js https://boards.4channel.org/qa/thread/3188516
```

It will screenshot the whole page of https://boards.4channel.org/qa/thread/3188516 every 10 seconds and stop when the HTTP code is not 200 or an error occurs.